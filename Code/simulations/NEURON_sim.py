# -*- coding: utf-8 -*-


#############################################################################
# To be run in  Sim4Life v6.2.0.4280, Neuron for Sim4Life v6.0.0.3176
#
#
#
# Authors: Andreas Rowald
# email: rowald.and@gmail.com
#
#############################################################################


### Importing relevant libraries
import os, sys, time, datetime, csv
import XCoreModeling
import numpy as np
import s4l_v1.document as doc
import s4l_v1.model as mdl
import s4l_v1.simulation.neuron as neuron
import s4l_v1.analysis as analysis
import s4l_v1.units as units

from s4l_v1.model import Unit
from s4l_v1.model import Vec3
from s4l_v1 import ReleaseVersion

def generateSegmentNames():
	a = []
	#for i in range(12,13):
		#a.append('T'+str(i))
	for i in range(1,6):
		a.append('L'+str(i))
	for i in range(1,3): #normally 5
		a.append('S'+str(i))
	return a

def extractDiametersFromTextFile(filepath):
	input = np.loadtxt(filepath, delimiter = '\n')
	return list(input)

def CreateNeuronSimulation(ents, diameter_dir, stim_pulse_path, source):

	sim = neuron.Simulation()
	sim.Name = 'Neuron_'+source

	### Setup
	sim.SetupSettings.GlobalTemperature = 37.0, Unit("C")
	sim.SetupSettings.PerformTitration = True
	sim.SetupSettings.TitrationConvergenceCriterion = 1.0
	sim.SetupSettings.DepolarizationDetection = sim.SetupSettings.DepolarizationDetection.enum.Threshold
	sim.SetupSettings.DepolarizationThreshold = 80.0*1E-3, units.Volts
	# Loop over segments
	segmentList = generateSegmentNames()
	quadrantList = ['DR','DL']
	n_fibers = 50
	for seg_idx, seg_val in enumerate(segmentList):
		for quad_idx, quad_val in enumerate(quadrantList):
			current_root = seg_val + '_' + quad_val

			fiber_group = ents[current_root]
			fiber_group_entities = fiber_group.Entities

			# Assign Type Ia/Ib fibers
			diameter_file = 'fiber_diameters_alpha.txt'
			current_diameter_path = os.path.join(diameter_dir,diameter_file)
			list_of_diameters = extractDiametersFromTextFile(current_diameter_path)
			for i in range(n_fibers):
				# Setup of fiber parameters
				axon_settings = neuron.SensoryMrgSettings()
				axon_settings.FiberDiameterReal = list_of_diameters[i]
				axon_settings.Name = str(i+1) + '_d_' + str(np.round(axon_settings.FiberDiameterReal,3))
				axon = [fiber_group_entities[i].Clone()]
				axon[0].Name = current_root + '_Aalpha_' + str(i)
				sim.Add(axon_settings,axon)


	sim.LoadModel()

	### Sources
	simlf = source
	incident_sim = doc.AllSimulations[simlf]
	source_settings = sim.AddSource(incident_sim, "Overall Field")
	source_settings.PulseType = source_settings.PulseType.enum.User
	source_settings.StimulusFilePath = stim_pulse_path

	### Solver settings
	solver_settings = sim.SolverSettings
	solver_settings.Parallelization = solver_settings.Parallelization.enum.Automatic
	solver_settings.NumberOfThreads = 1
	solver_settings.Duration = 10.0
	solver_settings.TimeStep = 0.0025

	return sim

def AnalyzeNeuroSimulation(simNeuro, ents, output_dir):
	''' Extract the results of titration, providing a list of data as
		in the titration sensor table.
	'''
	results = simNeuro.Results()
	titration_evaluator = analysis.neuron_evaluators.TitrationEvaluator()
	titration_evaluator.SetInputConnection(results["TitrationSensor"]["Titration"])
	titration_evaluator.Update(0)

	table = titration_evaluator.GetOutput(0)
	#lis = np.array(table.ToList())

	# titration_data for v5
	titration_data = titration_evaluator.TitrationData()

	# Iterate through roots
	segmentList = generateSegmentNames()
	quadrantList = ['DR','DL']
	Fiber_Type_List = ['Aalpha']
	number_fibers = range(50)

	for seg_idx, seg_val in enumerate(segmentList):
		for quad_idx, quad_val in enumerate(quadrantList):
			for type_dix, type_val in enumerate(Fiber_Type_List):
				current_root = seg_val + '_' + quad_val + '_' + type_val
				fiber_name = current_root
				neuron_list = []    # neuron names
				location_list = []  # sites of spike initiation
				latency_list = []   # latency (time of first spike)
				titration_list = [] # titration factors
				""" 
				for row in lis:
					if current_root in str(row[0]):
						neuron_list.append(str(row[0]))         # neuron name
						location_list.append(str(row[1]))       # site of spike initiation
						latency_list.append(str(row[2]))        # latency (time of first spike)
						titration_list.append(np.float(row[3])) # titration factor
				WriteTitrationToCSV(neuron_list,location_list,latency_list,titration_list,output_dir,fiber_name,simNeuro.Name)

				"""
				for row in titration_data:
					if current_root in str(row.NeuronName):
						neuron_list.append(str(row.NeuronName))                 # neuron name
						location_list.append(str(row.LocationOfFirstSpike))     # site of spike initiation
						latency_list.append(str(row.TimeOfFirstSpike))          # latency (time of first spike)
						titration_list.append(np.float(row.TitrationFactor))    # titration factor
				WriteTitrationToCSV(neuron_list,location_list,latency_list,titration_list,output_dir,fiber_name,simNeuro.Name)
	return 0


def WriteTitrationToCSV(neuron_list,location_list,latency_list,titration_list,output_dir,fiber_name,simname):

	'''
	Writes data from titration table to csv
	'''
	header = ['Neuron Name','Location of First Spike','Time of First Spike (ms)','Titration Factor']

	csv_folder = os.path.join(output_dir, 'csv')
	if not os.path.exists(csv_folder):
		os.makedirs(csv_folder)

	rows = zip(neuron_list,location_list,latency_list,titration_list)
	with open(os.path.join(csv_folder,'Titration_'+simname+'_'+fiber_name+'.csv'),'w') as f:
		w = csv.writer(f)
		w.writerow(header)
		for row in rows:
			w.writerow(row)

	return 0


def main(project_path,diameter_dir,stim_pulse_path, results_dir):

	sim_list = []
	Lead_positions = ["GO_0","GO_1","GO_2","GO_3","GO_4","GO_5"] #Which lead positions
	electrode_positions = ["E1","E5","E15","E11"]#Which electrodes

	for lead in Lead_positions:
		for elec in electrode_positions:
			source = 'Monopolar stim - '+ elec + '_' + elec + '_' + lead
			ents = mdl.AllEntities()
			output_dir = os.path.join(results_dir,'Results_'+elec+'_'+lead)
			if not os.path.exists(output_dir):
				os.makedirs(output_dir)

			simNeuro = CreateNeuronSimulation(ents, diameter_dir, stim_pulse_path,source)
			doc.AllSimulations.Add(simNeuro)
			simNeuro.RunSimulation(wait = True)
			AnalyzeNeuroSimulation(simNeuro, ents, output_dir)
	doc.SaveDocument()

	return 0

if __name__ == '__main__':
	# Start timing the whole script
	start_time = time.time()

	# Get ROOT path
	root_dir = r"..\Project_dir"
	# print root_dir
	# Get PROJECT path containing the main document
	project_dir = r"..\Project_dir"
	project_file = 'Example.smash'
	project_path = os.path.join(project_dir, project_file)
	# print project_dir
	# print project_file
	# print project_path

	# Get DIAMETER path for directory containing fiber diameter files
	diameter_dir = r'..\Project_dir\Input'
	# print diameter_dir

	# Get INPUT_PULSE directory
	stim_pulse_dir = r'..\Project_dir\Input'
	stim_pulse_file = r'typical_test_pulse.txt'
	stim_pulse_path = os.path.join(stim_pulse_dir, stim_pulse_file)
	# print stim_pulse_dir
	# print stim_pulse_file
	# print stim_pulse_path

	# Get RESULTS dir
	results_dir = r'..\Project_dir\Results'

	main(project_path,diameter_dir,stim_pulse_path, results_dir)
