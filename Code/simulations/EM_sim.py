# -*- coding: utf-8 -*-


#############################################################################
# To be run in  Sim4Life v6.2.0.4280
#
#
#
# Authors: Andreas Rowald
# email: rowald.and@gmail.com
#
#############################################################################


#%% Importing libraries
import os
from os.path import join
import time, datetime
import numpy as np
import s4l_v1.document as document
import s4l_v1.model as model
import s4l_v1.simulation.emlf as emlf
from s4l_v1 import ReleaseVersion, Unit
from s4l_v1.model import Vec3, Translation, Rotation
import s4l_v1.units as units
import s4l_v1.analysis as analysis
import XCoreModeling
#%% Subfunctions

def CreateRealSimulation(active_electrode,electrode_condition,lead_pos):
		"""
		Set up all the settings of the EM-LF simn
		"""
		print('Creating EM-LF simulation')
		# Creating the simulation
		sim2 = emlf.ElectroQsOhmicSimulation()
		# Name of Anisotropy
		sim2.Name = "Monopolar stim - " + electrode_condition + "_" + active_electrode+'_'+lead_pos

		# Mapping the components and entities
		component_plane_x = sim2.AllComponents["Plane X+"]
		component_plane_x_1 = sim2.AllComponents["Plane X-"]
		component_plane_y = sim2.AllComponents["Plane Y+"]
		component_plane_y_1 = sim2.AllComponents["Plane Y-"]
		component_plane_z = sim2.AllComponents["Plane Z+"]
		component_plane_z_1 = sim2.AllComponents["Plane Z-"]

		# Get all Entities
		ents = model.AllEntities()
		entity_wm = ents['SC_Rootlets']
		#entity_wm = ents['SC_NoRootlets']
		entity_lower = ents['Lower_Boundary']
		entity_upper = ents['Upper_Boundary']
		entity_saline = ents["Saline"]
		entity_bone = ents["BONE"]
		entity_fat = ents["FAT"]
		entity_csf = ents["CSF"]
		entity_gm = ents["GM"]
		entity_ipg_face = ents["IPG"]
		entity_disc = ents["DISC"]

		WM_comp = [e for e in entity_wm.Entities]
		WM = [e for e in WM_comp if 'WM' in e.Name]
		root_clusters = [e for e in WM_comp if 'Group' in e.Name]
		rootlets = [k for cluster in root_clusters for k in cluster.Entities]

		elec_folder = ents[lead_pos]
		elec_comp = [e for e in elec_folder.Entities if "Electrodes" in e.Name]
		act_folder = elec_comp[0]
		electrodes = [e for e in act_folder.Entities]

		paddle_comp = [e for e in elec_folder.Entities if "Paddle" in e.Name]
		entity_paddle = paddle_comp[0]


		### Setup
		sim2.SetupSettings.Frequency = 1000, units.Hz

		### Materials
		# Assign electric conductivity value to spinal cord entities

		### Materials
		material_settings = emlf.MaterialSettings()
		components = electrodes
		components = [e for e in components if e.Name!=active_electrode]
		material_settings.Name = "Electrodes"
		material_settings.MaterialType = material_settings.MaterialType.enum.PEC
		sim2.Add(material_settings, components)

		# Silicone Paddle
		material_settings = emlf.MaterialSettings()
		components = entity_paddle
		material_settings.Name = "Silicone"
		material_settings.ElectricProps.RelativePermittivity = 3.0
		sim2.Add(material_settings, components)

		# White matter
		material_settings = emlf.MaterialSettings()
		components = WM
		material_settings.Name = "WM"
		conductivity_algorithm = document.AllAlgorithms['Anisotropic Conductivity'+'_'+electrode_condition+'_'+lead_pos]
		wm_conductivity = conductivity_algorithm.Outputs['Anisotropic Conductivity From E Field']
		material_settings.SetInhomogeneousConductivity(wm_conductivity)
		sim2.Add(material_settings, components)

		# Rootlets
		material_settings = emlf.MaterialSettings()
		components = rootlets
		material_settings.Name = "Rootlets"
		conductivity_algorithm = document.AllAlgorithms['Anisotropic Conductivity'+'_'+electrode_condition+'_'+lead_pos]
		wm_conductivity = conductivity_algorithm.Outputs['Anisotropic Conductivity From E Field']
		material_settings.SetInhomogeneousConductivity(wm_conductivity)
		sim2.Add(material_settings, components)

		# Grey matter
		material_settings = emlf.MaterialSettings()
		components = [entity_gm]
		material_settings.Name = "GM"
		material_settings.ElectricProps.Conductivity = 0.23, Unit("S/m")
		sim2.Add(material_settings, components)

		# Cerebrospinal fluid
		material_settings = emlf.MaterialSettings()
		components = [entity_csf]
		material_settings.Name = "CSF"
		material_settings.ElectricProps.Conductivity = 1.7, Unit("S/m")
		sim2.Add(material_settings, components)

		# Epidural Fat
		material_settings = emlf.MaterialSettings()
		components = [entity_fat]
		material_settings.Name = "Fat"
		material_settings.ElectricProps.Conductivity = 0.04, Unit("S/m")
		sim2.Add(material_settings, components)

		# Bone
		material_settings = emlf.MaterialSettings()
		components = [entity_bone]
		material_settings.Name = "Bone"
		material_settings.ElectricProps.Conductivity = 0.02, Unit("S/m")
		sim2.Add(material_settings, components)
		# Disc
		material_settings = emlf.MaterialSettings()
		components = [entity_disc]
		material_settings.Name = "Disc"
		material_settings.ElectricProps.Conductivity = 0.65, Unit("S/m")
		sim2.Add(material_settings, components)
		# Saline
		material_settings = emlf.MaterialSettings()
		components = [entity_saline]
		material_settings.Name = "Saline"
		material_settings.ElectricProps.Conductivity = 2.0, Unit("S/m")
		sim2.Add(material_settings, components)

		# Pia
		#material_settings = emlf.MaterialSettings()
		#components = [entity_pia]
		#material_settings.Name = "Pia"
		#material_settings.ElectricProps.Conductivity = 0.05, Unit("S/m")
		#sim2.Add(material_settings, components)

		# Dura
		#material_settings = emlf.MaterialSettings()
		#components = [entity_dura]
		#material_settings.Name = "Dura"
		#material_settings.ElectricProps.Conductivity = 0.0025, Unit("S/m")
		#sim2.Add(material_settings, components)

		### Boundary Conditions
		boundary_settings = emlf.BoundarySettings()
		components = [e for e in electrodes if e.Name==active_electrode]
		boundary_settings.Name = "Active Site"
		boundary_settings.DirichletValue = 1.0, units.Volts
		sim2.Add(boundary_settings, components)

		boundary_settings = emlf.BoundarySettings()
		components = [entity_ipg_face]
		boundary_settings.Name = "Ground"
		boundary_settings.DirichletValue = 0.0, units.Volts
		sim2.Add(boundary_settings, components)

		### Sensors - Should be fine as is

		### Grid
		# Editing GlobalGridSettings "Grid (Empty)"
		global_grid_settings = sim2.GlobalGridSettings
		global_grid_settings.PaddingMode = global_grid_settings.PaddingMode.enum.Manual
		global_grid_settings.BottomPadding = np.array([1.0, 1.0, 1.0]), units.MilliMeters
		global_grid_settings.TopPadding = np.array([1.0, 1.0, 1.0]), units.MilliMeters

		# Adding a new AutomaticGridSettings

		manual_grid_settings = sim2.AddManualGridSettings([entity_bone,entity_disc])
		manual_grid_settings.Name = "Bone_Disc"
		manual_grid_settings.MaxStep = np.array([2, 2, 2]), units.MilliMeters


		# Adding a new AutomaticGridSettings
		manual_grid_settings = sim2.AddManualGridSettings([entity_fat, entity_csf])
		manual_grid_settings.Name = "Fat_CSF_GM"
		manual_grid_settings.MaxStep = np.array([1, 1, 1]), units.MilliMeters

		# Adding a new AutomaticGridSettings
		manual_grid_settings = sim2.AddManualGridSettings([entity_lower,entity_upper, entity_gm])
		manual_grid_settings.Name = "Boundaries"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for WM
		manual_grid_settings = sim2.AddManualGridSettings(WM)
		manual_grid_settings.Name = "WM"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for WM
		manual_grid_settings = sim2.AddManualGridSettings(rootlets)
		manual_grid_settings.Name = "Rootlets"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for electrodes
		manual_grid_settings = sim2.AddManualGridSettings(electrodes)
		manual_grid_settings.Name = "Electrodes"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for electrodes
		manual_grid_settings = sim2.AddManualGridSettings([entity_ipg_face])
		manual_grid_settings.Name = "IPG"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for electrodes
		manual_grid_settings = sim2.AddManualGridSettings([entity_paddle])
		manual_grid_settings.Name = "Paddle"
		manual_grid_settings.MaxStep = np.array([0.6, 0.6, 0.6]), units.MilliMeters

		# Course grid for saline
		components = [entity_saline]
		manual_grid_settings = sim2.AddManualGridSettings(components)
		manual_grid_settings.Name = "Saline_Grid"
		manual_grid_settings.MaxStep = np.array([10.0, 10.0, 10.0]), units.MilliMeters

		### Sensors
		# Record everything
		sensor_settings = next(x for x in sim2.AllSettings if x.Name == "Field Sensor Settings")

		### Voxels

		# Higher priority of SC than saline
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = [entity_saline]
		manual_voxeler_settings.Priority = 0
		sim2.Add(manual_voxeler_settings, components)

		# Higher priority of SC than saline
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = [entity_bone]
		manual_voxeler_settings.Priority = 1
		sim2.Add(manual_voxeler_settings, components)
		# Higher priority of SC than saline
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = [entity_disc]
		manual_voxeler_settings.Priority = 2
		sim2.Add(manual_voxeler_settings, components)
		# Higher priority of SC than saline
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = [entity_fat]
		manual_voxeler_settings.Priority = 3
		sim2.Add(manual_voxeler_settings, components)

		# Higher priority of SC than saline
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = [entity_csf]
		manual_voxeler_settings.Priority = 4
		sim2.Add(manual_voxeler_settings, components)

		# Higher priority of SC than saline
		#manual_voxeler_settings = emlf.ManualVoxelerSettings()
		#components = [entity_dura]
		#manual_voxeler_settings.Priority = 4
		#sim2.Add(manual_voxeler_settings, components)

		# Set Voxels for Rootlets
		base = 5
		max_n_fibers = 100
		visited = np.zeros((len(root_clusters),max_n_fibers))
		for priority in range(max_n_fibers):
			manual_voxeler_settings = emlf.ManualVoxelerSettings()
			manual_voxeler_settings.Priority = priority + base
			components = []
			for cluster in root_clusters:
				these = cluster.Entities
				try:
					root = these[priority]
					print(root.Name)
					components.append(root)
				except IndexError:
					continue
			sim2.Add(manual_voxeler_settings, components)

		# Higher priority of SC than saline
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = WM
		manual_voxeler_settings.Priority = base + max_n_fibers
		sim2.Add(manual_voxeler_settings, components)

		# Higher priority of SC than saline
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = [entity_gm]
		manual_voxeler_settings.Priority = base + max_n_fibers + 1
		sim2.Add(manual_voxeler_settings, components)

		# Higher priority of SC than saline
		#manual_voxeler_settings = emlf.ManualVoxelerSettings()
		#components = [entity_pia]
		#manual_voxeler_settings.Priority = 7
		#sim2.Add(manual_voxeler_settings, components)

		# 2nd Highest priority for electrodes
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = [entity_paddle]
		manual_voxeler_settings.Priority = base + max_n_fibers + 2
		sim2.Add(manual_voxeler_settings, components)
		# 2nd Highest priority for electrodes
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = electrodes
		manual_voxeler_settings.Priority = base + max_n_fibers + 3
		sim2.Add(manual_voxeler_settings, components)

		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = [entity_ipg_face]
		manual_voxeler_settings.Priority = base + max_n_fibers + 4
		#manual_voxeler_settings.RegionCharacteristic = manual_voxeler_settings.RegionCharacteristic.enum.Surface
		sim2.Add(manual_voxeler_settings, components)

		solver_settings = sim2.SolverSettings
		#solver_settings.PredefinedTolerances = solver_settings.PredefinedTolerances.enum.High
		solver_settings.AdditionalSolverOptions = u"-ksp_type gmres"

		### Solver
		sim2.SolverSettings.NumberOfProcesses = 8
		# sim.SolverSettings.Priority = 1

		print("Voxeller Set")

		print("SIMULATION SETTINGS - DONE")

		return sim2


def CreateLFSimulation(electrode_position,lead_pos):
		"""
		Set up all the settings of the EM-LF simn
		"""
		print('Creating EM-LF simulation')
		# Creating the simulation
		sim = emlf.ElectroQsOhmicSimulation()
		# Name of Anisotropy
		sim.Name = "Subject_Anisotropy"+'_'+electrode_position+'_'+lead_pos

		# Mapping the components and entities
		component_plane_x = sim.AllComponents["Plane X+"]
		component_plane_x_1 = sim.AllComponents["Plane X-"]
		component_plane_y = sim.AllComponents["Plane Y+"]
		component_plane_y_1 = sim.AllComponents["Plane Y-"]
		component_plane_z = sim.AllComponents["Plane Z+"]
		component_plane_z_1 = sim.AllComponents["Plane Z-"]

		# Get all Entities
		ents = model.AllEntities()
		entity_wm = ents['SC_Rootlets']
		#entity_wm = ents['SC_NoRootlets']
		entity_lower = ents['Lower_Boundary']
		entity_upper = ents['Upper_Boundary']
		entity_saline = ents["Saline"]
		entity_bone = ents["BONE"]
		entity_fat = ents["FAT"]
		entity_csf = ents["CSF"]
		entity_gm = ents["GM"]
		entity_ipg_face = ents["IPG"]
		entity_disc = ents["DISC"]

		WM_comp = [e for e in entity_wm.Entities]
		WM = [e for e in WM_comp if 'WM' in e.Name]
		root_clusters = [e for e in WM_comp if 'Group' in e.Name]
		rootlets = [k for cluster in root_clusters for k in cluster.Entities]

		elec_folder = ents[lead_pos]
		elec_comp = [e for e in elec_folder.Entities if "Electrodes" in e.Name]

		act_folder = elec_comp[0]
		electrodes = [e for e in act_folder.Entities]

		paddle_comp = [e for e in elec_folder.Entities if "Paddle" in e.Name]
		entity_paddle = paddle_comp[0]

		### Setup
		sim.SetupSettings.Frequency = 1000, units.Hz

		### Materials
		# Assign electric conductivity value to spinal cord entities
		material_settings = emlf.MaterialSettings()
		components = electrodes
		components = [e for e in components if e.Name!=electrode_position]
		material_settings.Name = "Electrodes"
		material_settings.MaterialType = material_settings.MaterialType.enum.PEC
		sim.Add(material_settings, components)

		# Silicone Paddle
		material_settings = emlf.MaterialSettings()
		components = entity_paddle
		material_settings.Name = "Silicone"
		material_settings.ElectricProps.RelativePermittivity = 3.0
		sim.Add(material_settings, components)

		# White matter
		material_settings = emlf.MaterialSettings()
		components = WM
		material_settings.Name = "WM"
		material_settings.ElectricProps.Conductivity = 1.0, Unit("S/m")
		sim.Add(material_settings, components)

		# Rootlets
		material_settings = emlf.MaterialSettings()
		components = rootlets
		material_settings.Name = "Rootlets"
		material_settings.ElectricProps.Conductivity = 1.0, Unit("S/m")
		sim.Add(material_settings, components)

		# Grey matter
		material_settings = emlf.MaterialSettings()
		components = [entity_gm]
		material_settings.Name = "GM"
		material_settings.ElectricProps.Conductivity = 0.23, Unit("S/m")
		sim.Add(material_settings, components)

		# Cerebrospinal fluid
		material_settings = emlf.MaterialSettings()
		components = [entity_csf]
		material_settings.Name = "CSF"
		material_settings.ElectricProps.Conductivity = 1.7, Unit("S/m")
		sim.Add(material_settings, components)

		# Epidural Fat
		material_settings = emlf.MaterialSettings()
		components = [entity_fat]
		material_settings.Name = "Fat"
		material_settings.ElectricProps.Conductivity = 0.04, Unit("S/m")
		sim.Add(material_settings, components)

		# Bone
		material_settings = emlf.MaterialSettings()
		components = [entity_bone]
		material_settings.Name = "Bone"
		material_settings.ElectricProps.Conductivity = 0.02, Unit("S/m")
		sim.Add(material_settings, components)
		# Disc
		material_settings = emlf.MaterialSettings()
		components = [entity_disc]
		material_settings.Name = "Disc"
		material_settings.ElectricProps.Conductivity = 0.65, Unit("S/m")
		sim.Add(material_settings, components)
		# Saline
		material_settings = emlf.MaterialSettings()
		components = [entity_saline]
		material_settings.Name = "Saline"
		material_settings.ElectricProps.Conductivity = 2.0, Unit("S/m")
		sim.Add(material_settings, components)

		### Boundary Conditions

		# Adding BoundarySettings for 1V at active site
		boundary_settings = emlf.BoundarySettings()
		components = [entity_lower]
		boundary_settings.Name = "Lower Boundary"
		boundary_settings.DirichletValue = 1.0, units.Volts
		sim.Add(boundary_settings, components)

		# Adding BoundarySettings for 0V at skin-air interface
		boundary_settings = emlf.BoundarySettings()
		components = [entity_upper]
		boundary_settings.Name = "Upper Boundary"
		boundary_settings.DirichletValue = 0.0, units.Volts
		sim.Add(boundary_settings, components)

		### Grid
		# Editing GlobalGridSettings "Grid (Empty)"
		global_grid_settings = sim.GlobalGridSettings
		global_grid_settings.PaddingMode = global_grid_settings.PaddingMode.enum.Manual
		global_grid_settings.BottomPadding = np.array([1.0, 1.0, 1.0]), units.MilliMeters
		global_grid_settings.TopPadding = np.array([1.0, 1.0, 1.0]), units.MilliMeters

		# Adding a new AutomaticGridSettings

		manual_grid_settings = sim.AddManualGridSettings([entity_bone,entity_disc])
		manual_grid_settings.Name = "Bone_Disc"
		manual_grid_settings.MaxStep = np.array([2, 2, 2]), units.MilliMeters


		# Adding a new AutomaticGridSettings
		manual_grid_settings = sim.AddManualGridSettings([entity_fat, entity_csf])
		manual_grid_settings.Name = "Fat_CSF_GM"
		manual_grid_settings.MaxStep = np.array([1, 1, 1]), units.MilliMeters

		# Adding a new AutomaticGridSettings
		manual_grid_settings = sim.AddManualGridSettings([entity_lower,entity_upper, entity_gm])
		manual_grid_settings.Name = "Boundaries"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for WM
		manual_grid_settings = sim.AddManualGridSettings(WM)
		manual_grid_settings.Name = "WM"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for WM
		manual_grid_settings = sim.AddManualGridSettings(rootlets)
		manual_grid_settings.Name = "Rootlets"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for electrodes
		manual_grid_settings = sim.AddManualGridSettings(electrodes)
		manual_grid_settings.Name = "Electrodes"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for electrodes
		manual_grid_settings = sim.AddManualGridSettings([entity_ipg_face])
		manual_grid_settings.Name = "IPG"
		manual_grid_settings.MaxStep = np.array([0.3, 0.3, 0.3]), units.MilliMeters

		# Manual fine grid for electrodes
		manual_grid_settings = sim.AddManualGridSettings([entity_paddle])
		manual_grid_settings.Name = "Paddle"
		manual_grid_settings.MaxStep = np.array([0.6, 0.6, 0.6]), units.MilliMeters
		# Course grid for saline
		components = [entity_saline]
		manual_grid_settings = sim.AddManualGridSettings(components)
		manual_grid_settings.Name = "Saline_Grid"
		manual_grid_settings.MaxStep = np.array([10.0, 10.0, 10.0]), units.MilliMeters

		### Sensors
		# Record everything
		sensor_settings = next(x for x in sim.AllSettings if x.Name == "Field Sensor Settings")

		### Voxels
		# Set Voxels for Rootlets
		base = 0
		max_n_fibers = 100
		visited = np.zeros((len(root_clusters),max_n_fibers))
		for priority in range(max_n_fibers):
			manual_voxeler_settings = emlf.ManualVoxelerSettings()
			manual_voxeler_settings.Priority = priority
			components = []
			for cluster in root_clusters:
				these = cluster.Entities
				try:
					root = these[priority]
					print(root.Name)
					components.append(root)
				except IndexError:
					continue
			sim.Add(manual_voxeler_settings, components)

		# Set Voxels for WM
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = WM
		manual_voxeler_settings.Priority = base + max_n_fibers
		sim.Add(manual_voxeler_settings, components)

		# Set Voxels for Boundaries
		manual_voxeler_settings = emlf.ManualVoxelerSettings()
		components = [entity_lower,entity_upper]
		manual_voxeler_settings.Priority = base + max_n_fibers + 1
		#manual_voxeler_settings.RegionCharacteristic = manual_voxeler_settings.RegionCharacteristic.enum.Surface
		sim.Add(manual_voxeler_settings, components)

		solver_settings_1 = sim.SolverSettings
		#solver_settings_1.PredefinedTolerances = solver_settings_1.PredefinedTolerances.enum.High
		solver_settings_1.AdditionalSolverOptions = u"-ksp_type gmres"

		print("SIMULATION SETTINGS - DONE")

		return sim

def RunLFSimulation(sim, project_path):
	"""
	Run EM simulation and save project
	"""
	# Update the materials with the new frequency parameters
	sim.UpdateAllMaterials()
	# Update the grid with the new parameters
	sim.UpdateGrid()
	sim.CreateVoxels(project_path)
	# Add the simulation to the UI
	document.AllSimulations.Add(sim)
	print('Running EM simulation')
	sim.RunSimulation(wait = True)
	print("Simulation Done")
	document.SaveDocument()
	print("Document Saved")



def CreateMockAnisotropicConductivity(sim,electrode_position,lead_pos):
	# Transversal and longitudinal sigmas
	sigma1 = 0.6
	sigma2 = 0.083

	# here conductivity has 6 component (anisotropic tensor)
	results = sim.Results()
	p = results["Overall Field"].Outputs["EM Potential(x,y,z,f0)"]
	p.Update()
	e = results["Overall Field"].Outputs["EM E(x,y,z,f0)"]
	e.Update()

	e_field = e.Data.Field(0)
	nc = e.Data.Grid.NumberOfCells
	#print "E Field shape", e_field.shape
	#print "Number of Cells", nc
	# E Field shape (nc, 3) - Cell centered

	# Calculate sigma for white matter on cell centers
	print("Calculate anisotropic sigma values")
	sigma_wm = np.zeros([nc,6]) # 6 unique values
	# tensor component indexing for solver
	# Simone below
	iu3 = (np.array([0, 1, 2, 0, 1, 0], dtype=np.int64), np.array([0, 1, 2, 1, 2, 2], dtype=np.int64))
	# Esra below
	# iu3 = (np.array([0, 1, 2, 0, 0, 1], dtype=np.int64), np.array([0, 1, 2, 1, 2, 2], dtype=np.int64))
	for idx, val in enumerate(e_field):
		tmp = np.zeros([3,3], dtype=np.complex)
		tmp[0] = val
		if (np.isnan(val).any()):
			P = np.full((3, 3), 0, dtype=np.complex)
			#P = np.full((3, 3), np.nan, dtype=np.complex)
		else:
			if (np.inner(val, val) != 0):
				P = np.dot(tmp.T, tmp) / np.inner(val,val)
			else:
				P = np.full((3, 3), 0, dtype=np.complex)
		sigma_tensor = sigma1*P + sigma2*( np.identity(P.shape[0]) - P )
		sigma_wm[idx] = sigma_tensor[iu3].real
		#print sigma_wm[idx]
		#sigma_wm[idx] = sigma_tensor.real
	tensor_conductivity = analysis.core.DoubleFieldData()
	tensor_conductivity.NumberOfSnapshots = 1
	tensor_conductivity.NumberOfComponents = 6
	tensor_conductivity.Grid = e.Data.Grid.Clone()
	tensor_conductivity.ValueLocation = analysis.core.eValueLocation.kCellCenter
	tensor_conductivity.SetField(0, sigma_wm)
	tensor_conductivity.Quantity.Name = 'Anisotropic Conductivity From E Field'
	tensor_conductivity.Quantity.Unit = analysis.core.Unit('S/m')

	producer_t = analysis.core.TrivialProducer()
	producer_t.SetDataObject(tensor_conductivity)
	producer_t.Name = 'Anisotropic Conductivity'+'_'+electrode_position+'_'+lead_pos
	document.AllAlgorithms.Add(producer_t)


def main(project_path):

	Lead_decision = "GO_2_Leads" #Which lead would you like to use
	Lead_positions = ["GO_0","GO_1","GO_2","GO_3","GO_4","GO_5"] #Which lead positions
	electrode_positions = ["E1","E5","E15","E11"]#Which electrodes
	for pos in Lead_positions:
		for elec in electrode_positions:
			#First simulation is to generate a field from which we may extrapolate anisotropic conductivities
			sim = CreateLFSimulation(elec,pos)
			sim.UpdateAllMaterials()
			sim.UpdateGrid()
			sim.CreateVoxels(project_path)
			document.AllSimulations.Add(sim)
			sim.RunSimulation(wait = True)
			#Now calculate anistropic conductivities
			CreateMockAnisotropicConductivity(sim,elec,pos)
			document.SaveDocument()
			#Now run actual simulation
			sim2 = CreateRealSimulation(elec,elec,pos)
			sim2.UpdateAllMaterials()
			sim2.UpdateGrid()
			sim2.CreateVoxels(project_path)
			document.AllSimulations.Add(sim2)
			sim2.RunSimulation(wait = True)
	document.SaveDocument()

	print("ALL DONE!")
	return 0


if __name__ == '__main__':
	# Start timing the whole script
	start_time = time.time()
	#Directory of the smash-file
	project_dir = r"..\Project_dir"
	#Name of the smash-file
	fname = 'Example.smash'
	#Creates the project path
	project_path = os.path.join(project_dir, fname)

	### Call main() function
	main(project_path)
	# Display total time of execution for this script
	print("Total time of execution: ", datetime.timedelta(seconds=(time.time() - start_time)))
	print("Simulation DONE")
