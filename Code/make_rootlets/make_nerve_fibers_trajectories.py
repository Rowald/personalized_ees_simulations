# -*- coding: utf-8 -*-


#############################################################################
# Module: .../make_rootlets.py
#
# Created on: 19 August 2019
#
# Authors: Andreas Rowald, Nathan Greiner
# email: rowald.and@gmail.com
#
#############################################################################


import os
import numpy as np
import re

from Plotter import Plotter
import geom


def make_nerve_fiber_trajectories(input_folder_path, output_folder_path, **kwargs):
    """ Build and export coordinates of nerve fiber trajectories. """

    plot = False
    if plot:
        plotter = Plotter()

    # Extract parameters from keyword arguments.
    segs = kwargs['segs']
    pdens = kwargs['pdens']
    nfibers = kwargs['nfibers']

    # Load textfile data.
    root_CS = dict()
    file_names = os.listdir(input_folder_path)
    for seg in segs:
        root_CS[seg] = dict()
        for pos in ['DL', 'DR', 'VL', 'VR']:
            root_CS[seg][pos] = dict()
            root_CS[seg][pos]['main'] = list()
            nCS = len([x for x in file_names if re.match('{:}_{:}_main_CS.*'.format(seg, pos), x)])
            for k in range(nCS):
                tmp = np.loadtxt(os.path.join(input_folder_path, '{:}_{:}_main_CS_{:d}.txt'.format(seg, pos, k + 1)))
                root_CS[seg][pos]['main'].append(tmp)
            nrlets = len([x for x in file_names if re.match('{:}_{:}_rlet.*GC.*'.format(seg, pos), x)])
            root_CS[seg][pos]['rlets'] = list()
            for i in range(nrlets):
                root_CS[seg][pos]['rlets'].append(list())
                nCS = len([x for x in file_names if re.match('{:}_{:}_rlet_{:d}_CS.*'.format(seg, pos, i + 1), x)])
                for k in range(nCS):
                    tmp = np.loadtxt(os.path.join(input_folder_path, '{:}_{:}_rlet_{:d}_CS_{:d}.txt'.format(seg, pos, i + 1, k + 1)))
                    root_CS[seg][pos]['rlets'][-1].append(tmp)

    # Build nerve fiber trajectories.
    root_fibers = dict()
    for seg in segs:
        root_fibers[seg] = dict()
        for pos in ['DL', 'DR', 'VL', 'VR']:
            root_fibers[seg][pos] = list()
            for rlet_CSs in root_CS[seg][pos]['rlets']:
                tmp = [build_constrained_path(root_CS[seg][pos]['main'] + rlet_CSs, pdens) for _ in range(nfibers)]
                root_fibers[seg][pos].append(tmp)

    # Plot fibers.
    if plot:
        for seg in segs:
            for pos in ['DL', 'DR', 'VL', 'VR']:
                for rlet_CSs, fibers in zip(root_CS[seg][pos]['rlets'], root_fibers[seg][pos]):
                    fig, axs = plotter.build_figure(1, projection='3d')
                    for CS in root_CS[seg][pos]['main']:
                        plotter.plot_3d_lines(axs[0], CS[:, 0], CS[:, 1], CS[:, 2])
                    for CS in rlet_CSs:
                        plotter.plot_3d_lines(axs[0], CS[:, 0], CS[:, 1], CS[:, 2])
                    for fiber in fibers:
                        plotter.plot_3d_lines(axs[0], fiber[:, 0], fiber[:, 1], fiber[:, 2])
                    plotter.format_ax(axs[0], aspect='equal3d')
                    plotter.show()
                    input()
                    plotter.close(fig)


    # Export nerve fiber coordinates.
    try:
        os.mkdir(output_folder_path)
    except:
        pass

    for i, seg in enumerate(segs):
        for pos in ['DL', 'DR', 'VL', 'VR']:
            for k, fibers in enumerate(root_fibers[seg][pos]):
                for n, fiber in enumerate(fibers):
                    np.savetxt(os.path.join(output_folder_path, '{:}_{:}_rlet_{:d}_fiber_{:d}.txt'.format(seg, pos, k + 1, n + 1)), fiber)


def build_unconstrained_path(CS, pdens, starting_point=None):
    """ Build an unconstrained path running through the input cross-sections CS. """

    # Recover number of cross-sections.
    nCS = len(CS)

    # Generate interpolation points.
    intP = np.zeros((nCS, 3))
    for i in range(nCS):
        nP = CS[i].shape[0]
        # Generate weights for the weighted sums.
        w = geom.gen_weights_unit_simplex(nP, 3)
        # Form weighted sum.
        intP[i, :] = np.matmul(w, CS[i])

    # Add starting point if provided in varargin.
    if starting_point is not None:
        intP = np.concatenate([starting_point, intP], axis=0)

    # Sample points along a cubic spline interpolating the interpolation points.
    nP = int(round(geom.get_arc_length(intP) * pdens))
    path = geom.interparc(nP, intP)

    return path


def build_constrained_path(CS, pdens, starting_point=None):
    """ Build a constrained path running through the input cross-sections CS. """

    # Recover number of cross-sections.
    nCS = len(CS)

    # Recover number of points in each cross-section.
    nP = CS[0].shape[0]

    # Generate weights for the weighted sums.
    w = geom.gen_weights_unit_simplex(nP, order=5)

    # Generate a weighted sum in each cross-section using the previous
    # weights.
    intP = np.zeros((nCS, 3))
    for i in range(nCS):
        intP[i, :] = np.matmul(w, CS[i])

    # Add starting point if provided in varargin.
    if starting_point is not None:
        intP = np.concatenate([starting_point, intP], axis=0)

    # Sample points along a cubic spline interpolating the previous points.
    nP = int(round(geom.get_arc_length(intP) * pdens))
    path = geom.interparc(nP, intP)

    return path



if __name__ == '__main__':

    input_folder_path = '..\input_folder_roots_centerlines_as_txt'
    output_folder_path = '..\output_folder_nerve_fibers_as_txt'

    params = dict()
    params['segs'] = ['T12', 'L1', 'L2', 'L3', 'L4', 'L5', 'S1', 'S2', 'S3', 'S4', 'S5']
    params['pdens'] = 1.5
    params['nfibers'] = 10

    make_nerve_fiber_trajectories(input_folder_path, output_folder_path, **params)
