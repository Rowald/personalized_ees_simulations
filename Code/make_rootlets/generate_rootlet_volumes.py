# -*- coding: utf-8 -*-
#%% Import libraries
from os.path import join
import sys
sys.path.append('../gran')
import os
import re
import numpy as np
import XCoreModeling
import s4l_v1.model as model
from s4l_v1.model import Vec3, Translation, Rotation
import s4l_v1.document as document

def LoftSplines(spline_list):
	"""
	This function creates a volumetric body interpolating a list of contours
	"""
	body = XCoreModeling.SkinWires(spline_list)
	for entity in spline_list:
		entity.Delete()
	return body

def main(root_folder):

	for file in os.listdir(root_folder):
		points = np.loadtxt(os.path.join(root_folder,file), unpack=False)
		listOfPoints = [Vec3(p[0],p[1],p[2]) for p in points]
		contour = model.CreateSpline(listOfPoints)
		contour.Name = file[:-4]


	list_of_roots = ['T12','L1','L2','L3','L4','L5','S1','S2']
	quad_of_roots = ['DL','DR','VL','VR']
	cond_of_roots = ['main','rlet']

	dir_list = os.listdir(root_folder)

	ents = model.AllEntities()
	for root in list_of_roots:
		for quad in quad_of_roots:
			for cond in cond_of_roots:
				current_root = root + '_' + quad + '_' + cond + '_CS_'
				counter = 0
				for entry in dir_list:
					if current_root in entry:
						counter = counter + 1
				contour_list = []
				for i in range(1,counter):
					current_contour = ents[current_root + str(i)]
					contour_list.append(current_contour)
					body = LoftSplines(contour_list)
					body.Name = current_root




if __name__ == '__main__':

	root_folder = r'C:\Users\admin\Documents\Publications\STIMO_BRIDGE\Fiber_Recruitment\GJ10_nerve_fiber_coords_2'
	#main(root_folder)



	for file in os.listdir(root_folder):
		points = np.loadtxt(os.path.join(root_folder,file), unpack=False)
		listOfPoints = [Vec3(p[0],p[1],p[2]) for p in points]
		contour = model.CreateSpline(listOfPoints)
		contour.Name = file[:-4]
