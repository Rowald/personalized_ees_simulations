# -*- coding: utf-8 -*-


#############################################################################
# Module: .../geom.py
#
# Created on: 28 August 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np
from scipy import interpolate


def get_evenly_spaced_idxs(curve, dens, nmin=4):
    """ Select evenly-spaced idxs along curve. """

    # Number of points on curve.
    L = curve.shape[0]
    
    # Number of idxs to distribute.
    nidxs = int(dens * get_arc_length(curve))
    nidxs = max(nmin, nidxs)
    nidxs = min(L, nidxs)

    # Select idxs.
    idxs = np.zeros(nidxs, dtype=int);
    q = L / (nidxs - 1)
    r = int(0.5 * (L % (nidxs - 1)))
    
    # The first idx should be 0.
    idxs[0] = 0
    
    # The last idx should be the last idx of the curve.
    idxs[-1] = L - 1
    
    # The other ones should be evenly spaced.
    for i in range(1, nidxs - 1):
        idxs[i] = q * i + r;

    return idxs


def get_arc_length(curve):
    """ Return arc length of 2D or 3D curve. """
    return np.sum(np.sqrt(np.sum(np.square(np.diff(curve, axis=0)), axis=1)), axis=0)


def rotate_curve(curve, alpha):
    """ Rotate curve in the xy-plane. """
    
    c = np.cos(alpha);
    s = np.sin(alpha);
    
    if curve.shape[1] == 2:
        rot_mat = np.array([[c, -s],
                            [s,  c]])
        
    else:
        rot_mat = np.array([[c, -s,  0],
                            [s,  c,  0],
                            [0,  0,  1]])
            
    return np.matmul(rot_mat, curve.T).T


def build_circle(diam, npoints=40):
    """ Build a 3D circle in the xy-plane. """

    circle = np.zeros((npoints, 3))
    t = np.linspace(0, 2 * np.pi, num=npoints)
    circle[:, 0] = 0.5 * diam * np.cos(t)
    circle[:, 1] = 0.5 * diam * np.sin(t)
    
    return circle


def bend_circle(C, u):
    """ Bend input 3D circle in the direction u. """
    
    # Define u2, v and theta (cumulated rotation angle).
    u2 = u.copy()
    u2[2] = 0.0
    v = np.asarray([-u[1], u[0], 0.0])
    v = v / np.linalg.norm(v)
    
    # Determine rotation angle.
    # Retrieve unoriented angle between u and u2.
    tmp = np.dot(u, u2) / np.linalg.norm(u) / np.linalg.norm(u2)
    if abs(tmp - 1.0) < 1e-10: tmp = 1.0
    if abs(tmp + 1.0) < 1e-10: tmp = -1.0
    theta = np.arccos(tmp)
    # Orient it according to 3rd coordinate of u.
    if u[2] > 0.0:
        theta = -theta
    # Rotation angle.
    theta = theta - 0.5 * np.pi

    # Form rotation matrix.
    v1 = v[0]
    v2 = v[1]
    c = np.cos(theta)
    s = np.sin(theta)
    rot_mat = np.array([[c + v1**2 * (1 - c),    v1 * v2 * (1 - c),  -v2 * s],
                        [  v1 * v2 * (1 - c),  c + v2**2 * (1 - c),   v1 * s],
                        [             v2 * s,              -v1 * s,        c]])
    
    # Perform rotation.
    return np.matmul(C, rot_mat)


def interparc(ninter, pxyz):
    """
    Sample ninter points along a spline interpolating the curve pxyz.

    Description: see https://ch.mathworks.com/matlabcentral/...
    ...fileexchange/34874-interparc
    """

    # how many points will be interpolated?
    ninit = pxyz.shape[0]

    # preallocate the result, pt
    pt = np.zeros((ninter, 3))

    # Compute the chordal (linear) arclength
    # of each segment. This will be needed for
    # any of the methods.
    chordlen = np.sqrt(np.sum(np.square(np.diff(pxyz, n=1, axis=0)), axis=1)).reshape(ninit - 1, 1)

    # Normalize the arclengths to a unit total
    chordlen = chordlen / np.sum(chordlen, axis=0)

    # cumulative arclength
    cumarc = np.cumsum(chordlen, axis=0)
    cumarc = np.insert(cumarc, 0, 0.)
    newcumarc = np.linspace(0, cumarc[-1], num=ninter)

    # Interpolate
    for i in range(3):
      sp = interpolate.CubicSpline(cumarc, pxyz[:, i])
      pt[:, i] = sp(newcumarc)

    return pt


def gen_weights_unit_simplex(d, order=1):
    """ Return a vector w sampled uniformly at random in the unit simplex of R^d. """
    
    # Generate weights from the unit simplex.
    w = np.random.random(size=(1, d + 1))
    w[0, 0] = 0.0
    w[0, -1] = 1.0
    w = np.sort(w, axis=1)
    w = np.diff(w, axis=1)
    
    # Exponentiate if necessary.
    if order != 1:
        w = np.power(w, order)
        w = w / np.sum(w)

    return w
