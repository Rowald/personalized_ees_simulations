# -*- coding: utf-8 -*-


#############################################################################
# Module: .../make_rootlets.py
#
# Created on: 19 August 2019
#
# Authors: Andreas Rowald, Nathan Greiner
# email: rowald.and@gmail.com
#
#############################################################################


import os
import numpy as np
import re

from Plotter import Plotter
import geom


def make_rootlets(input_folder_path, output_folder_path, **kwargs):
    """ Build and exports coordinates of guide curves of rootlets. """

    plot = False
    if plot:
        plotter = Plotter()

    # Extract parameters from keyword arguments.
    segs = kwargs['segs']
    diam_rlet = kwargs['diam_rlet']
    dmin = kwargs['dmin']
    nrlets_max = kwargs['nrlets_max']
    pdens = kwargs['pdens']
    rsplits = kwargs['rsplits']

    # Load textfile data.
    input_data = dict()
    for seg in segs:
        input_data[seg] = dict()
        for pos in ['DL', 'DR', 'VL', 'VR']:
            tmp = np.loadtxt(os.path.join(input_folder_path, '{:}_{:}.txt'.format(seg, pos)))
            input_data[seg][pos] = geom.interparc(int(pdens * geom.get_arc_length(tmp)), tmp)

    # Build rootlet coordinates.
    root_coords = dict()
    for i, seg in enumerate(segs):

        # The last segment is discarded from the process.
        if i == len(segs) - 1:
            continue

        root_coords[seg] = dict()

        # Form coordinates for all roots of current segment.
        for pos in ['DL', 'DR', 'VL', 'VR']:
            root_coords[seg][pos] = dict()

            # Retrieve z-coordinates of the tips of the root of the current segment and of the root of the next segment.
            zfc = input_data[seg][pos][-1, 2]
            zic = input_data[seg][pos][0, 2]
            zfn = input_data[segs[i + 1]][pos][-1, 2]

            # The splitting in rootlets happens around a specific height between zfc and zic.
            rsplit = rsplits[seg][pos]
            zsplit = zic + rsplit * (zfc - zic)

            # Find index of the point of the root of the current segment where splitting in rootlets should occur.
            # Extract its z-coordinate.
            idx = np.argmin(np.abs(input_data[seg][pos][:, 2] - zsplit))
            zsplit = input_data[seg][pos][idx, 2]

            # Form common branch coordinates.
            root_coords[seg][pos]['main'] = input_data[seg][pos][0 : idx + 1, :]

            # Determine number of rootlets and spacing between rootlets.
            L = (zfn - zfc) + diam_rlet
            nrlets = int((L + dmin) / (diam_rlet + dmin))
            nrlets = min(nrlets, nrlets_max)
            d = (L - nrlets * diam_rlet) / (nrlets - 1)

            # Form rootlet coordinates.
            root_coords[seg][pos]['rlets'] = list()
            for k in range(nrlets):
                zn = zfc + (k - 1) * (diam_rlet + d)
                tmp = input_data[seg][pos][idx :, :].copy()
                tmp[:, 2] = (tmp[:, 2] - zsplit) * (zn - zsplit) / (zfc - zsplit) + zsplit
                root_coords[seg][pos]['rlets'].append(tmp)

            # Plot roots.
            if plot:
                fig, axs = plotter.build_figure(1, projection='3d')
                plotter.plot_3d_lines(axs[0], root_coords[seg][pos]['main'][:, 0], root_coords[seg][pos]['main'][:, 1], root_coords[seg][pos]['main'][:, 2])
                for rlet in root_coords[seg][pos]['rlets']:
                    plotter.plot_3d_lines(axs[0], rlet[:, 0], rlet[:, 1], rlet[:, 2], **params)
                plotter.format_ax(axs[0], aspect='equal3d')
                raw_input()
                plotter.close(fig)


    # Export rootlet coordinates.
    try:
        os.mkdir(output_folder_path)
    except:
        pass

    for i, seg in enumerate(segs):

        # The last segment is discarded from the process.
        if i == len(segs) - 1:
            continue

        for pos in ['DL', 'DR', 'VL', 'VR']:
            np.savetxt(os.path.join(output_folder_path, '{:}_{:}_main_GC.txt'.format(seg, pos)), root_coords[seg][pos]['main'])
            for k, rlet in enumerate(root_coords[seg][pos]['rlets']):
                np.savetxt(os.path.join(output_folder_path, '{:}_{:}_rlet_{:d}_GC.txt'.format(seg, pos, k + 1)), rlet)


def make_root_cross_sections(folder_path, **kwargs):
    """ Build and export cross-section coordinates of roots. """

    plot = False
    if plot:
        plotter = Plotter()

    # Extract parameters from keyword arguments.
    segs = kwargs['segs']
    diam_root = kwargs['diam_root']
    diam_rlet = kwargs['diam_rlet']
    dens = kwargs['dens']

    # Load textfile data.
    root_GC = dict()
    file_names = os.listdir(folder_path)
    for seg in segs:
        root_GC[seg] = dict()
        for pos in ['DL', 'DR', 'VL', 'VR']:
            root_GC[seg][pos] = dict()
            root_GC[seg][pos]['main'] = np.loadtxt(os.path.join(folder_path, '{:}_{:}_main_GC.txt'.format(seg, pos)))
            nrlets = len([x for x in file_names if re.match('{:}_{:}_rlet.*GC.*'.format(seg, pos), x)])
            root_GC[seg][pos]['rlets'] = list()
            for k in range(nrlets):
                tmp = np.loadtxt(os.path.join(folder_path, '{:}_{:}_rlet_{:d}_GC.txt'.format(seg, pos, k + 1)))
                root_GC[seg][pos]['rlets'].append(tmp)

    # Build root cross-sections.
    root_CS = dict()
    for seg in segs:
        root_CS[seg] = dict()
        for pos in ['DL', 'DR', 'VL', 'VR']:
            root_CS[seg][pos] = dict()
            tmp1, tmp2 = build_cross_sections_compound_curve(root_GC[seg][pos]['main'], root_GC[seg][pos]['rlets'][0], dens=dens, d1=diam_root, d2=diam_rlet)
            root_CS[seg][pos]['main'] = tmp1
            root_CS[seg][pos]['rlets'] = [tmp2]
            root_CS[seg][pos]['rlets'] += [build_cross_sections(x, dens=dens, d1=0.95*diam_root, d2=diam_rlet) for x in root_GC[seg][pos]['rlets'][1 :]]

    # Plot roots.
    if plot:
        for seg in segs:
            for pos in ['DL', 'DR', 'VL', 'VR']:
                fig, axs = plotter.build_figure(1, projection='3d')
                # plotter.plot_3d_lines(axs[0], root_GC[seg][pos]['main'][:, 0], root_GC[seg][pos]['main'][:, 1], root_GC[seg][pos]['main'][:, 2], **params)
                # for CS in root_CS[seg][pos]['main']:
                #     plotter.plot_3d_lines(axs[0], CS[:, 0], CS[:, 1], CS[:, 2])
                for rlet_GC, rlet_CSs in zip(root_GC[seg][pos]['rlets'], root_CS[seg][pos]['rlets']):
                    plotter.plot_3d_lines(axs[0], rlet_GC[:, 0], rlet_GC[:, 1], rlet_GC[:, 2])
                    for CS in rlet_CSs:
                        plotter.plot_3d_lines(axs[0], CS[:, 0], CS[:, 1], CS[:, 2])
                    plotter.format_ax(axs[0], aspect='equal3d')
                    raw_input()
                plotter.close(fig)


    # Export cross-section coordinates.
    try:
        os.mkdir(output_folder_path)
    except:
        pass

    for i, seg in enumerate(segs):
        for pos in ['DL', 'DR', 'VL', 'VR']:
            for n, CS in enumerate(root_CS[seg][pos]['main']):
                np.savetxt(os.path.join(folder_path, '{:}_{:}_main_CS_{:d}.txt'.format(seg, pos, n + 1)), CS)
            for k, rlet in enumerate(root_CS[seg][pos]['rlets']):
                for n, CS in enumerate(rlet):
                    np.savetxt(os.path.join(folder_path, '{:}_{:}_rlet_{:d}_CS_{:d}.txt'.format(seg, pos, k + 1, n + 1)), CS)


def build_cross_sections(curve, **kwargs):

    # Extract parameters from keyword arguments.
    dens = kwargs['dens']
    d1 = kwargs['d1']
    d2 = kwargs['d2']

    # Convenient variables.
    x = curve[:, 0]
    y = curve[:, 1]
    z = curve[:, 2]

    # Get idxs of points which will serve as centers for the cross-sections.
    idxs = geom.get_evenly_spaced_idxs(curve, dens)
    nCS = len(idxs)

    # Initialize cross-sections array.
    CSs = list()

    # Build cross-sections.
    for i in range(nCS):

        # The center of the CS.
        center = curve[idxs[i], :]

        # The 1st CS should be orthogonal to the vector joining the 1st two
        # points of the curve.
        if i == 0:
            u1 = x[1] - x[0]
            u2 = y[1] - y[0]
            u3 = z[1] - z[0]
        # The last CS should be orthogonal to the vector joining the last
        # two points of the curve. In fact, we want it to be vertical, so the
        # 3rd coordinate of this vector should be imposed =0.
        elif i == nCS - 1:
            u1 = x[-1] - x[-2]
            u2 = y[-1] - y[-2]
            u3 = 0.
        # Any other CS should be orthogonal to the vector joining the
        # two points surrounding the CS center on the curve.
        else:
            u1 = x[idxs[i] + 1] - x[idxs[i] - 1]
            u2 = y[idxs[i] + 1] - y[idxs[i] - 1]
            u3 = z[idxs[i] + 1] - z[idxs[i] - 1]

        # Set u and beta.
        u = np.asarray([u1, u2, u3])
        beta = np.arccos(u1 / np.linalg.norm(np.asarray([u1, u2])))
        if u2 < 0:
            beta = -beta
        # print(u)
        # raw_input()

        # Build CS.
        # It is a circle lying in a plane orthogonal to vector u,
        # centered around the point center.

        # Circle diameter.
        d = d1 + (d2 - d1) * float(i) / float(nCS - 1)

        # Build, orient and move circle at appropriate location.
        CS = geom.build_circle(d)
        CS = geom.rotate_curve(CS, beta)
        CS = geom.bend_circle(CS, u)
        CS = CS + center
        CSs.append(CS)

    return CSs


def build_cross_sections_compound_curve(curve1, curve2, **kwargs):

    # Extract parameters from keyword arguments.
    dens = kwargs['dens']
    d1 = kwargs['d1']
    d2 = kwargs['d2']

    # Get idxs of points which will serve as centers for the cross-sections.
    idxs1 = geom.get_evenly_spaced_idxs(curve1, dens)
    idxs2 = geom.get_evenly_spaced_idxs(curve2, dens)[1 :]
    curve = np.concatenate((curve1, curve2[1 :]), axis=0)
    idxs = np.concatenate((idxs1, idxs2 + curve1.shape[0] - 1))
    nCS = len(idxs)

    # Convenient variables.
    x = curve[:, 0]
    y = curve[:, 1]
    z = curve[:, 2]

    # Initialize cross-sections array.
    CSs = list()

    # Build cross-sections.
    for i in range(nCS):

        # The center of the CS.
        center = curve[idxs[i], :]

        # The 1st CS should be orthogonal to the vector joining the 1st two
        # points of the curve.
        if i == 0:
            u1 = x[1] - x[0]
            u2 = y[1] - y[0]
            u3 = z[1] - z[0]
        # The last CS should be orthogonal to the vector joining the last
        # two points of the curve. In fact, we want it to be vertical, so the
        # 3rd coordinate of this vector should be imposed =0.
        elif i == nCS - 1:
            u1 = x[-1] - x[-2]
            u2 = y[-1] - y[-2]
            u3 = 0.
        # Any other CS should be orthogonal to the vector joining the
        # two points surrounding the CS center on the curve.
        else:
            u1 = x[idxs[i] + 1] - x[idxs[i] - 1]
            u2 = y[idxs[i] + 1] - y[idxs[i] - 1]
            u3 = z[idxs[i] + 1] - z[idxs[i] - 1]

        # Set u and beta.
        u = np.asarray([u1, u2, u3])
        beta = np.arccos(u1 / np.linalg.norm(np.asarray([u1, u2])))
        if u2 < 0:
            beta = -beta
        # print(u)
        # raw_input()

        # Build CS.
        # It is a circle lying in a plane orthogonal to vector u,
        # centered around the point center.

        # Circle diameter.
        if idxs[i] <= idxs1[-1]:
            d = d1
        else:
            d = d1 + (d2 - d1) * float(idxs[i] - idxs1[-1]) / float(idxs[-1] - idxs1[-1])

        # Build, orient and move circle at appropriate location.
        CS = geom.build_circle(d)
        CS = geom.rotate_curve(CS, beta)
        CS = geom.bend_circle(CS, u)
        CS = CS + center
        CSs.append(CS)

    CSs1 = CSs[0 : len(idxs1) - 1]
    CSs2 = CSs[len(idxs1) - 1 :]

    return CSs1, CSs2


if __name__ == '__main__':

    # # Make rootlets.
    input_folder_path = r'..\input_folder_roots_centerlines_as_txt'
    output_folder_path = r'..\output_folder_roolets_centerlines_and_crosssections_as_txt'
    params = dict()
    params['segs'] = ['T12', 'L1', 'L2', 'L3', 'L4', 'L5', 'S1', 'S2', 'S3']
    params['diam_rlet'] = 0.3
    params['dmin'] = 0.3
    params['nrlets_max'] = 15
    params['pdens'] = 1.5
    params['rsplits'] = dict()
    params['rsplits']['T12'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    params['rsplits']['L1'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    params['rsplits']['L2'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    params['rsplits']['L3'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    params['rsplits']['L4'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    params['rsplits']['L5'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    params['rsplits']['S1'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    params['rsplits']['S2'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    #params['rsplits']['S3'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    #params['rsplits']['S4'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    #params['rsplits']['S5'] = dict([(pos, 0.5) for pos in ['DL', 'DR', 'VL', 'VR']])
    make_rootlets(input_folder_path, output_folder_path, **params)

    # Make root cross-sections.
    # folder_path = r'C:\Users\admin\Documents\Publications\STIMO_BRIDGE\GJ10\Roots_out'
    # params = dict()
    # params['segs'] = ['T12', 'L1', 'L2', 'L3', 'L4', 'L5', 'S1', 'S2']
    # params['diam_root'] = 0.6
    # params['diam_rlet'] = 0.3
    # params['dens'] = 1.5
    # make_root_cross_sections(folder_path, **params)
